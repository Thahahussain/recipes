import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ItemComponent } from './item/item.component';
import { MasterComponent } from './master/master.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RecipesComponent } from './recipes/recipes.component';
import { TagsComponent } from './tags/tags.component';
import { TemplateComponent } from './template/template.component';

const routes: Routes = [
  { path: "", redirectTo: "/home", pathMatch: "full" },
  { path: "home", component: MasterComponent },
  { path: "about", component: AboutComponent },
  { path: "tags", component: TagsComponent },
  { path: "contact", component: ContactComponent },
  { path: "recipes", component: RecipesComponent },
  { path: "item", component: ItemComponent },
  {path:"recipeslist",component: TemplateComponent},
  {path:"**",component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
