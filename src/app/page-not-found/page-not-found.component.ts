import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <main>
      <section class="error-page">
        <h1>404</h1>
        <h3>Error Page</h3>
      </section>
      
    </main>
  `,
  styles: [`
  .error-page{
      text-align: center;
      padding-top: 5rem;
    }
    .error-page h1{
      font-size: 9rem;
    }`
    
  ]
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
